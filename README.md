Installation:

	git clone git@bitbucket.org:vinspee/my-vim-config.git
	git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle

Create symlinks:

    ln -s ~/.vim/.vimrc ~/.vimrc

Launch vim, run ````:BundleInstall```` (or vim +BundleInstall +qall for CLI lovers)
