" .vimrc File
" Maintained by: Vince Speelman
" v@vinspee.me

"""""""""""""
" CORE SETUP
"""""""""""""

" Forget compatibility with Vi. Who cares.
set nocompatible
filetype off

" VUNDLE CONFIG
"""""""""""""""

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#rc()

" let Vundle manage Vundle
" required!
Bundle 'gmarik/vundle'

" My Bundles here:
" original repos on github

" Deps
""""""""""""""""
Bundle 'MarcWeber/vim-addon-mw-utils'
Bundle 'tomtom/tlib_vim'

" Configuration
""""""""""""""""
Bundle 'editorconfig/editorconfig-vim'

" Display
""""""""""""""""
Bundle 'altercation/vim-colors-solarized'
Bundle 'nathanaelkane/vim-indent-guides'
Bundle 'bilalq/lite-dfm'
Bundle 'bling/vim-airline'
"Bundle 'gorodinskiy/vim-coloresque'

" Version Control
""""""""""""""""""
Bundle 'airblade/vim-gitgutter'
Bundle 'mattn/webapi-vim.git'
Bundle 'mattn/gist-vim'
" Copy Gist URL
let g:gist_clip_command = 'pbcopy'
Bundle 'tpope/vim-fugitive'
Bundle 'idanarye/vim-merginal'

" Syntax
""""""""""""""""
Bundle 'scrooloose/syntastic'
Bundle 'docunext/closetag.vim'
Bundle 'Raimondi/delimitMate'
Bundle 'edsono/vim-matchit'
"Bundle 'itspriddle/vim-jquery'
Bundle 'pangloss/vim-javascript'
"Bundle 'jelera/vim-javascript-syntax'
Bundle 'leshill/vim-json'
Bundle 'kchmck/vim-coffee-script'
Bundle 'groenewege/vim-less'
Bundle 'othree/html5.vim'
Bundle 'tpope/vim-haml'
Bundle 'tpope/vim-liquid'
Bundle 'tpope/vim-markdown'
Bundle 'itspriddle/vim-marked'
Bundle 'wavded/vim-stylus'
Bundle 'mustache/vim-mustache-handlebars'
Bundle 'digitaltoad/vim-jade'
Bundle 'mxw/vim-jsx'
Bundle 'JulesWang/css.vim'

" Search
""""""""""""""""
Bundle 'kien/ctrlp.vim'
Bundle 'rking/ag.vim'

" Syntax
""""""""""""""""""
filetype plugin indent on
syntax on
syntax enable
let g:marked_app = "Marked"

" Editing Helpers
"""""""""""""""""""
Bundle 'godlygeek/tabular'
"Bundle 'bkad/CamelCaseMotion'
"map w <Plug>CamelCaseMotion_w
"map b <Plug>CamelCaseMotion_b
"map e <Plug>CamelCaseMotion_e
"sunmap w
"sunmap b
"sunmap e
"omap iw <Plug>CamelCaseMotion_iw
"xmap iw <Plug>CamelCaseMotion_iw
"omap ib <Plug>CamelCaseMotion_ib
"xmap ib <Plug>CamelCaseMotion_ib
"omap ie <Plug>CamelCaseMotion_ie
"xmap ie <Plug>CamelCaseMotion_ie
Bundle 'mattn/emmet-vim'
Bundle 'tpope/vim-surround'
Bundle 'vim-scripts/DrawIt'
Bundle 'terryma/vim-multiple-cursors'

" Completion Sugar
""""""""""""""""""""
"Bundle 'jordwalke/AutoComplPop'
"Bundle 'jordwalke/VimCompleteLikeAModernEditor'
"Bundle 'SirVer/ultisnips'
"Bundle 'honza/vim-snippets'
Bundle 'Shougo/neocomplete.vim'
Bundle 'Shougo/neosnippet'
Bundle 'Shougo/neosnippet-snippets'

" Place snippets in ~/.vim/myUltiSnippets/

" Status Line
""""""""""""""""
let g:bufferline_echo=0
let g:airline_theme='solarized'
set background=dark
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled = 1

" Tmux
""""""""""""""""
Bundle 'benmills/vimux'
Bundle 'edkolev/tmuxline.vim'
Bundle 'christoomey/vim-tmux-navigator'

" Utilities
""""""""""""""""
Bundle 'kana/vim-scratch'
nmap <F8> :ScratchOpen<CR>
Bundle 'scrooloose/nerdcommenter'
Bundle 'sjl/gundo.vim'
" Macros
""""""""""""""""
Bundle 'vim-scripts/DirDo.vim'



"Optimize for terminals
set ttyfast

"Show command in bottom right portion of the screen
set showcmd

"Show lines numbers
set number

"Hide mouse when typing
set mousehide
set mouse=a

"Split windows below the current window.
set splitbelow

"Prettify Vertical Splitter
set fillchars=vert:│ " that's a vertical box-drawing character

" Configure Tmux to not be ugly
set t_Co=256
hi Search cterm=NONE ctermfg=white ctermbg=blue

" Indent stuff
set smartindent
set autoindent

"let g:indent_guides_enable_on_vim_startup = 1

" Paste mode indent toggle
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

" Allow deletion of tabs
set backspace=indent,eol,start

" Always show the status line
set laststatus=2

" Prefer a slightly higher line height
set linespace=4

" Better line wrapping
set wrap
set linebreak
set breakat=79
set textwidth=79
set formatoptions=qrn1

" Fix Searching
"nnoremap / /\v
"vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
nnoremap <leader><space> :noh<cr>
nnoremap <tab> %
vnoremap <tab> %

" Open a Quickfix window for the last search.
nnoremap <silent> ,/ :execute 'vimgrep /'.@/.'/g %'<CR>:copen<CR>

" Show hidden chars like textmate
set list
set listchars=tab:▷⋅,trail:⋅,nbsp:⋅

" Make pasteboard work with tmux / osx
set clipboard+=unnamed

" Move tmp files to a specific location
set dir=~/tmp
set backup
set backupdir=~/tmp

" Skip Intro
set shortmess=atI

" Write the old file out when switching between files.
set autowrite

" Display current cursor position in lower right corner.
set ruler

" Hilight current line
set cursorline

" Ever notice a slight lag after typing the leader key + command? This lowers
" the timeout.
set timeoutlen=500

" Switch between buffers without saving
set hidden

" Completion
"set wildmenu
"set wildmode=list:longest

" Note: This option must set it in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return neocomplete#close_popup() . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? neocomplete#close_popup() : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplete#close_popup()
inoremap <expr><C-e>  neocomplete#cancel_popup()
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

" For cursor moving in insert mode(Not recommended)
"inoremap <expr><Left>  neocomplete#close_popup() . "\<Left>"
"inoremap <expr><Right> neocomplete#close_popup() . "\<Right>"
"inoremap <expr><Up>    neocomplete#close_popup() . "\<Up>"
"inoremap <expr><Down>  neocomplete#close_popup() . "\<Down>"
" Or set this.
"let g:neocomplete#enable_cursor_hold_i = 1
" Or set this.
"let g:neocomplete#enable_insert_char_pre = 1

" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1

" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:neocomplete#enable_auto_select = 1
"let g:neocomplete#disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
"let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
"let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
"let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'

"""""""""""""""""""
" Session Settings
set sessionoptions=resize,winpos,winsize,buffers,tabpages,folds,curdir,help


" FOLDING
"""""""""""""""
set foldmethod=indent

"Fold Coffee
au BufNewFile,BufReadPost *.coffee setl foldmethod=indent

"Fold Stylus
function! StylusFold()
	setl foldmethod=indent
	setl foldlevelstart=1
	setl foldnestmax=2
	setl foldminlines=5
	setl fen
endfunction
au FileType stylus call StylusFold()


function! JavaScriptFold()
	setl foldmethod=syntax
	setl foldlevelstart=1
	syn region foldBraces start=/{/ end=/}/ transparent fold keepend extend

	function! FoldText()
		return substitute(getline(v:foldstart), '{.*', '{...}', '')
	endfunction
	setl foldtext=FoldText()
endfunction
au FileType javascript call JavaScriptFold()
au FileType javascript setl fen


" CONFIGURATION
"""""""""""""""""

"Want a different map leader than \
let mapleader = ","

"Set font type and size. Depends on the resolution. Larger screens, prefer h20
set guifont="Source Code Pro":h15

" Tabs Rock.
set tabstop=2 shiftwidth=2 softtabstop=2 noexpandtab
nnoremap <leader>T :set tabstop=2 shiftwidth=2 softtabstop=2 noexpandtab<cr>

" But some people insist on using spaces. They're not smart.
nnoremap <leader>S :set tabstop=2 shiftwidth=2 softtabstop=2 expandtab<cr>

"Shortcut for editing  vimrc file in a new tab
nmap <leader>ev :e $MYVIMRC<cr>

" Source the vimrc file after saving it. This way, you don't have to reload Vim to see the changes.
if has("autocmd")
	augroup myvimrchooks
		au!
		autocmd bufwritepost .vimrc source ~/.vimrc
	augroup END
endif

"Faster shortcut for commenting. Requires T-Comment plugin
map <leader>c <c-_><c-_>

"Select function
nnoremap <leader>B ?function<cr>jva{Vo{

if v:version >= 703
	"undo settings
	set undodir=~/.vim/undofiles
	set undofile
	set colorcolumn=+1 "mark the ideal max text width
endif

"Delete all buffers (via Derek Wyatt)
nmap <silent> ,da :exec "1," . bufnr('$') . "bd"<cr>

" Closes the current buffer
nnoremap <silent> <Leader>q :bd<CR>

" Save with ,`
nnoremap <Leader>w :w<CR>

" Ctrl-h,j,k,l for split navigation
map <C-H> <C-W>h
map <C-L> <C-W>l
map <C-J> <C-W>j
map <C-K> <C-W>k

" Use + and - to size splits

nnoremap <silent> + :res +10<CR>
nnoremap <silent> - :res -10<CR>

" resize horzontal split window
nmap <leader><Left> <C-W>-<C-W>-
nmap <leader><Right> <C-W>+<C-W>+

" resize vertical split window
nmap <C-Up> <C-W>><C-W>>
nmap <C-Down> <C-W><<C-W><

" CtrlP
let g:ctrlp_map = '<c-p>'
let g:ctrlp_working_path_mode = 2


let g:ctrlp_custom_ignore = '\.git$\|\.hg$\|\.svn$|\.DS_Store$\'

" Remove toolbars
set guioptions=egt
set guioptions-=Tmr

" Automatically Reload Externally Modified CSS Files
augroup vimrcAu
	au!
	au BufEnter,BufNew *.css setlocal autoread
augroup END

"set JSX FT
autocmd! BufEnter  *.jsx  let b:syntastic_checkers=['jsxhint']


" ----------------------- "
" Custom Leader Functions "
" ----------------------- "


" Align to inputted char
vnoremap <leader>A :<c-u>execute ":'<,'>Tabular /".nr2char(getchar())<cr>

"Ag
nnoremap <leader>a :Ag 

"Fold Tag
nnoremap <leader>ft Vatzf

"Hardwrap
nnoremap <leader>w gqip

" Don't Show Default Mode. Airline does.
set noshowmode

"Select Pasted Text
nnoremap <leader>v V`]

" Super Retab
nmap <leader>rt :let @/=''<CR>:%retab!<CR>

" Remove Whitespace
:nnoremap <silent> <leader>ws :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

"Navigate Buffers w/ leader
noremap <leader>z :bprev!<CR>
noremap <leader>x :bnext!<CR>
noremap <leader>d :confirm bd<CR>

nnoremap <Leader>H :call<SID>LongLineHLToggle()<cr>
hi OverLength ctermbg=none cterm=none
match OverLength /\%>80v/
fun! s:LongLineHLToggle()
	if !exists('w:longlinehl')
		let w:longlinehl = matchadd('ErrorMsg', '.\%>80v', 0)
		echo "Long lines highlighted"
	else
		call matchdelete(w:longlinehl)
		unl w:longlinehl
		echo "Long lines unhighlighted"
	endif
endfunction

" Entify
" Escape/unescape & < > HTML entities in range (default current line).
function! HtmlEntities(line1, line2, action)
	let search = @/
	let range = 'silent ' . a:line1 . ',' . a:line2
	if a:action == 0	" must convert &amp; last
		execute range . 'sno/&lt;/</eg'
		execute range . 'sno/&gt;/>/eg'
		execute range . 'sno/&amp;/&/eg'
	else							" must convert & first
		execute range . 'sno/&/&amp;/eg'
		execute range . 'sno/</&lt;/eg'
		execute range . 'sno/>/&gt;/eg'
	endif
	nohl
	let @/ = search
endfunction
command! -range -nargs=1 Entities call HtmlEntities(<line1>, <line2>, <args>)
noremap <silent> <leader>e :Entities 0<CR>
noremap <silent> <leader>E :Entities 1<CR>


" Completion
"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsJumpForwardTrigger="<tab>"
"let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

let g:agprg="ag --column"

let g:ctrlp_show_hidden = 1

set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/.tmp/*,*/.sass-cache/*,*/node_modules/*,*.keep,*.DS_Store,*/.git/*

nnoremap <silent> <Leader>df <Plug>VimroomToggle
"This unsets the "last search pattern" register by hitting return
nnoremap <CR> :noh<CR><CR>

set shell=/bin/bash
